package nl.utwente.di.bookQuote;
import nl.utwente.di.bookQuote.Translator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/** Tests the quoter */
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Translator translator = new Translator();
        double fahrenheit = translator.getTempTranslator("0");
        assertEquals(0.0,fahrenheit,32.0);
    }
}
