package nl.utwente.di.bookQuote;
import java.util.HashMap;

public class Translator {


    public double getTempTranslator(String cTemp) {
        float fahrenheit = (Float.parseFloat(cTemp) * 9/5) + 32;
        return fahrenheit;
    }
}
